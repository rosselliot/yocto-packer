#!/bin/bash

# Exit on any error
set -eux

# Add vagrant user
useradd -U vagrant

# Set user password
echo vagrant:vagrant | chpasswd

# Setup vagrant insecure private key
mkdir -m 700 /home/vagrant/.ssh
curl https://raw.githubusercontent.com/mitchellh/vagrant/master/keys/vagrant.pub >> /home/vagrant/.ssh/authorized_keys
chmod 600 /home/vagrant/.ssh/authorized_keys
chown -R vagrant:vagrant /home/vagrant/.ssh

# Give vagrant password-less sudo access
echo "vagrant ALL=(ALL) NOPASSWD: ALL" > /etc/sudoers.d/vagrant

# Set UseDNS to no in /etc/ssh/sshd_config
# See https://github.com/hashicorp/vagrant/issues/9232
sed -i "s/#UseDNS.*/UseDNS no/g" /etc/ssh/sshd_config

sync

history -c
